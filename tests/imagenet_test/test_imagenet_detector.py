import matplotlib.pyplot as plt
import numpy as np

from eye_retinopathy.Services import *

if __name__ == '__main__':

    image1 = plt.imread('shovel.png')
    image1 = np.array(image1)

    image2 = plt.imread('bananas.png')
    image2 = np.array(image2)

    image = [image1, image2]

    inception = InceptionV3()
    detector = ImageNetDetector(inception)
    predicts = detector.get_predict(image)
    for predict in predicts:
        print(predict.head())