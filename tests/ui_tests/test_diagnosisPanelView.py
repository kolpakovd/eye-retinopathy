from unittest import TestCase

import sys
from eye_retinopathy.Controllers import DiagnosisPanelController
from PyQt5.QtWidgets import QApplication


class TestControlPanelView(TestCase):

    def test_show_window(self):
        QApplication(sys.argv)
        controller = DiagnosisPanelController(None, None)
        controller.view.show()
