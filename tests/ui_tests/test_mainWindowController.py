from unittest import TestCase

import sys
from eye_retinopathy.Controllers import MainWindowController
from PyQt5.QtWidgets import QApplication



class TestMainWindowController(TestCase):

    def test_show_window(self):
        APP = QApplication(sys.argv)
        controller = MainWindowController(None)
        controller.view.show()
        try:
            sys.exit(APP.exec_())
        except SystemExit:
            return
