from unittest import TestCase

import sys
from eye_retinopathy.Controllers import ControlPanelController
from PyQt5.QtWidgets import QApplication


class TestControlPanelView(TestCase):

    def test_show_window(self):
        APP = QApplication(sys.argv)
        controller = ControlPanelController(None)
        controller.view.show()
        try:
            sys.exit(APP.exec_())
        except SystemExit:
            return


