from unittest import TestCase

from PyQt5.QtWidgets import QApplication

import sys
from eye_retinopathy.Controllers import MainWindowController
from eye_retinopathy.Models import Content


class TestMain(TestCase):
   def test_functions(self):
       APP = QApplication(sys.argv)
       content = Content()
       MainWindowController(content)
       try:
           sys.exit(APP.exec_())
       except SystemExit:
           self.assertEqual(len(content.get_eyes()), 2)
