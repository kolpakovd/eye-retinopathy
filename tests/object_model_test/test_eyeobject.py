from unittest import TestCase

from eye_retinopathy.Models import EyeObject


class TestEyeObject(TestCase):
    def test_not_exists_file(self):
        eye = EyeObject(r'..\imagenet_test\shovel.jpg')
        results = eye()
        self.assertEqual(results, None)

    def test_exists_file(self):
        eye = EyeObject(r'..\imagenet_test\shovel.png')
        results = eye()
        self.assertEqual(type(results), tuple)
