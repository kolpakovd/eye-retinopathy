from eye_retinopathy.Models import *

from unittest import TestCase


class TestContent(TestCase):
    def test_get_hr_diagnosis(self):
        eye1 = EyeObject(r'..\imagenet_test\shovel.png')
        eye2 = EyeObject(r'..\imagenet_test\bananas.png')

        content = Content()
        content.add_eye(eye1)

        result1 = content.get_hr_diagnosis()
        self.assertEqual(type(result1), str)
        print(result1)

        content.add_eye(eye2)
        result1 = content.get_hr_diagnosis()
        self.assertEqual(type(result1), str)
        print(result1)

    def test_add_eye(self):
        eye1 = EyeObject(r'..\imagenet_test\shovel.png')
        eye2 = EyeObject(r'..\imagenet_test\shovel.jpg')

        content = Content()
        content.add_eye(eye1)
        content.add_eye(eye2)
        self.assertEqual(len(content.get_eyes()), 1)
        content.reset()
        self.assertEqual(len(content.get_eyes()), 0)
