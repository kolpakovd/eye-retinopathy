from PIL import Image
import numpy as np

from ..Services import DETECTOR

assert DETECTOR is not None


class EyeObject:

    def _handle(self):

            img = Image.open(self.file_name).convert('RGB')
            img = img.resize((299, 299), Image.ANTIALIAS)
            self.image = np.array(img, np.float)

            if max(self.image.flatten()) > 1:
                self.image /= 255

            diagnosis = DETECTOR.get_predict(self.image)
            self.diagnosis = list(diagnosis)[0]

    def __init__(self, file_name):
        self.file_name = file_name
        self.image = None
        self.diagnosis = None
        self._handle()

    def __call__(self, *args, **kwargs):
        if self.diagnosis is not None:
            return self.file_name, self.image, self.diagnosis
        else:
            return None


class Content:

    def get_hr_diagnosis(self, ):
        """
        Get human readable diagnosis
        :return: human readable diagnosis
        """
        if self._eyes:
            if not self._diagnosis:
                self._convert_diagnosis_to_hr()
            return self._diagnosis
        else:
            return None

    def _convert_diagnosis_to_hr(self):
        result_text = ''
        for _eye in self.get_eyes():
            result_text += _eye.file_name.replace('/','\\')+'\n\n'
            result_text += 'Probability \t Class'
            guess_text = ''.join([f"\n{row['probability']:.4f} \t {row['class']}" for _, row in _eye.diagnosis.head().iterrows()])
            result_text += guess_text + '\n\n\n\n'
        self._diagnosis = result_text

    def add_eye(self, eye):
        if type(eye) is EyeObject and eye not in self._eyes and type(eye()) is tuple:
            for _eye in self._eyes:
                if _eye.file_name == eye.file_name:
                    return
            self._eyes.append(eye)
            self._diagnosis = None

    def get_eyes(self):
        return frozenset(self._eyes)

    def reset(self):
        self._diagnosis = None
        self.selected_eye = None
        self._eyes = []

    def __init__(self, ):
        self._diagnosis = None
        self._eyes = []
        self.selected_eye = None
