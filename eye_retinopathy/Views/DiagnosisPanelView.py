from PyQt5.QtWidgets import QWidget

from ..Unilities import Observer
from ..UI.DiagnosisPanel import Ui_DiagnosisPanel


class DiagnosisPanelView(QWidget, Observer):

    def __init__(self, in_model, in_controller, parent=None):
        super(QWidget, self).__init__(parent)

        self.parent = parent
        self.model = in_model
        self.controller = in_controller

        self.ui = Ui_DiagnosisPanel()
        self.ui.setupUi(self)

    def model_is_changed(self):
        diagnosis = self.model.get_hr_diagnosis()
        self.ui.txt_diagnosis.setText(diagnosis)
