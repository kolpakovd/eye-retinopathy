from PyQt5.QtWidgets import QWidget

from ..Unilities import Observer
from ..UI import Ui_ControlPanel


class ControlPanelView(QWidget, Observer):

    def __init__(self, in_model, in_controller, parent=None):
        super(QWidget, self).__init__(parent)

        self.parent = parent
        self.model = in_model
        self.controller = in_controller

        self.ui = Ui_ControlPanel()
        self.ui.setupUi(ControlPanel=self)

        self.ui.btn_clear_list.clicked.connect(self.controller.clear_list_clicked)
        self.ui.btn_diagnose.clicked.connect(self.controller.diagnose_clicked)
        self.ui.btn_load_image.clicked.connect(self.controller.load_image_clicked)
        self.ui.btn_save_diagnosis.clicked.connect(self.controller.save_diagnosis_clicked)

    def model_is_changed(self):
        pass
