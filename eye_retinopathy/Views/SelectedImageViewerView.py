from PyQt5.QtWidgets import QWidget, QGraphicsView, QGraphicsPixmapItem, QGraphicsScene, QFrame, QGraphicsSceneDragDropEvent
from PyQt5.QtGui import QIcon, QPixmap, QDropEvent
from PyQt5.QtCore import QRectF, Qt

from ..Unilities import Observer
from ..UI.SelectedImageViewer import Ui_SelectedImageViewer


class DroppedScene(QGraphicsScene):

    def __init__(self, parent, path_hook):
        super(DroppedScene, self).__init__(parent)
        self.hook = path_hook

    def dragEnterEvent(self, e):
        e.acceptProposedAction()

    def dropEvent(self, e: QGraphicsSceneDragDropEvent):
        for url in e.mimeData().urls():
            self.hook(url.toString())

    def dragMoveEvent(self, e):
        e.acceptProposedAction()


class ImageViewer(QGraphicsView):
    def __init__(self, parent, model):
        super(ImageViewer, self).__init__(parent=parent)
        self._zoom = 100
        self._empty = True
        self._scene = DroppedScene(self, model)
        self._photo = QGraphicsPixmapItem()
        self._scene.addItem(self._photo)
        self.setScene(self._scene)

        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorUnderMouse)
        self.setFrameShape(QFrame.NoFrame)

        self._zoom_hint = None

    def has_photo(self):
        return not self._empty

    def fitInView(self, rect: QRectF = None, mode: Qt.AspectRatioMode = None):
        rect = QRectF(self._photo.pixmap().rect())
        if not rect.isNull():
            self.setSceneRect(rect)
            if self.has_photo():
                unity = self.transform().mapRect(QRectF(0, 0, 1, 1))
                self.scale(1 / unity.width(), 1 / unity.height())

    def set_photo(self, pixmap=None):
        self._zoom = 100
        if pixmap and not pixmap.isNull():
            self._empty = False
            self.setDragMode(QGraphicsView.ScrollHandDrag)
            self._photo.setPixmap(pixmap)
        else:
            self._empty = True
            self.setDragMode(QGraphicsView.NoDrag)
            self._photo.setPixmap(QPixmap())
        self.fitInView()
        self.scale(1, 1)

    def clear_photo(self):
        self._empty = True
        self._zoom = 100
        self._zoom_hint(self._zoom)

        self.scale(0.01, 0.01)
        self.set_photo()

    def wheelEvent(self, event):
        if self.has_photo():
            if event.angleDelta().y() > 0:
                factor = 1.1
                self._zoom += 10
            else:
                factor = 0.9
                self._zoom -= 10
            if self._zoom == 100:
                self.fitInView()
            elif self._zoom > 200:
                self._zoom = 200
            elif self._zoom < 20:
                self._zoom = 20
            else:
                self.scale(factor, factor)
            self._zoom_hint(self._zoom)

    def zoom(self, step, factor):
        self._zoom = step
        if self._zoom == 100:
            self.fitInView()
        elif self._zoom > 200:
            self._zoom = 200
        elif self._zoom < 20:
            self._zoom = 20
        else:
            self.scale(factor, factor)

    def set_zoom_hint(self, hint):
        self._zoom_hint = hint


class SelectedImageViewerView(QWidget, Observer):

    def __init__(self, in_model, in_controller, parent=None):
        super(SelectedImageViewerView, self).__init__(parent)

        self.parent = parent
        self.model = in_model
        self.controller = in_controller

        self.ui = Ui_SelectedImageViewer()
        self.ui.setupUi(self)

        self.setup_icons()

        self.ui.slider_zoom.valueChanged.connect(self.controller.changed_slider_value)
        self.ui.btn_zoom_p.clicked.connect(self.controller.zoom_p_clicked)
        self.ui.btn_zoom_m.clicked.connect(self.controller.zoom_m_clicked)

        self.ui.verticalLayout.removeWidget(self.ui.gv_image)
        self.ui.gv_image.destroy()
        self.ui.gv_image = ImageViewer(self.ui.gb_viewer, self.controller.add_eye_hook)
        self.ui.verticalLayout.insertWidget(0, self.ui.gv_image)
        self.ui.gv_image.repaint()
        self.repaint()
        self.ui.gv_image.set_zoom_hint(self.zoom_hint)

    def setup_icons(self):
        zoom_p = QIcon()
        zoom_p.addPixmap(QPixmap(r".\Resources\zoom_p.png"), QIcon.Normal, QIcon.Off)
        self.ui.btn_zoom_p.setIcon(zoom_p)
        self.ui.btn_zoom_p.setFlat(True)

        zoom_m = QIcon()
        zoom_m.addPixmap(QPixmap(r".\Resources\zoom_m.png"), QIcon.Normal, QIcon.Off)
        self.ui.btn_zoom_m.setIcon(zoom_m)
        self.ui.btn_zoom_m.setFlat(True)

    def model_is_changed(self):
        if self.model.selected_eye:
            img = QPixmap(self.model.selected_eye.file_name)
            self.ui.gv_image.set_photo(img)
        else:
            self.ui.gv_image.clear_photo()

    def zoom_hint(self, value):
        self.ui.slider_zoom.setValue(value)

    def zoom_reversed_hint(self, step, factor):
        self.ui.gv_image.zoom(step, factor)
