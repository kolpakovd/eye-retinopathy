from .ControlPanelView import ControlPanelView
from .DiagnosisPanelView import DiagnosisPanelView
from .ImagesViewerView import ImagesViewerView
from .MainWindowView import MainWindowView
from .SelectedImageViewerView import SelectedImageViewerView
from .FileDialogView import FileDialogView
