from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QVBoxLayout, QSplitter, QWidget, QGridLayout
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon, QPixmap

import sys

from ..Unilities import Observer
from ..UI import Ui_MainWindow


class MainWindowView(QMainWindow, Observer):

    def __init__(self, in_model, in_controller, parent=None):
        super(QMainWindow, self).__init__(parent)

        self.parent = parent
        self.model = in_model
        self.controller = in_controller

        self.ui = Ui_MainWindow()
        self.ui.setupUi(MainWindow=self)

        self.setup_icon()
        self.setup_menu()
        self.statusBar().show()
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ingore()

    def dropEvent(self, event):
        for url in event.mimeData().urls():
            self.controller.add_eye(url.toString())

    def setup_menu(self):
        file_menu = self.menuBar().addMenu('File')
        diagnostics_menu = self.menuBar().addMenu('Diagnostics')

        load_image = file_menu.addAction('Load image')
        load_image.triggered.connect(self.controller.load_image_clicked)

        clear_list = file_menu.addAction('Clear list')
        clear_list.triggered.connect(self.controller.clear_list_clicked)

        file_menu.addSeparator()

        exit_action = file_menu.addAction('Exit')
        exit_action.setShortcut('Ctrl+Q')
        exit_action.setStatusTip('Exit application')
        exit_action.triggered.connect(sys.exit)

        diagnose = diagnostics_menu.addAction('Diagnose')
        diagnose.triggered.connect(self.controller.diagnose_clicked)

        save_diagnosis = diagnostics_menu.addAction('Save diagnosis')
        save_diagnosis.triggered.connect(self.controller.save_diagnosis_clicked)

    def setup_panels(self):
        vertical_layout = QVBoxLayout()
        vertical_layout.setContentsMargins(0, 0, 0, 0)

        v_r_splitter = QSplitter(Qt.Vertical)
        v_r_splitter.addWidget(self.controller.selected_image_viewer_controller.view)
        v_r_splitter.addWidget(self.controller.diagnosis_panel_controller.view)
        v_r_splitter.setStretchFactor(0, 6)
        v_r_splitter.setStretchFactor(1, 4)
        v_r_splitter.setContentsMargins(0, 0, 0, 0)

        vertical_layout.addWidget(v_r_splitter)
        vertical_layout.addWidget(self.controller.control_panel_controller.view)

        right_widget = QWidget()
        right_widget.setLayout(vertical_layout)

        h_splitter = QSplitter(Qt.Horizontal)
        h_splitter.addWidget(self.controller.images_viewer_controller.view)
        h_splitter.addWidget(right_widget)
        h_splitter.setStretchFactor(0, 2)
        h_splitter.setStretchFactor(1, 5)
        h_splitter.setContentsMargins(0, 0, 0, 0)

        central_layout = QGridLayout()
        central_layout.addWidget(h_splitter)

        self.ui.centralwidget.setLayout(central_layout)

    def setup_icon(self):
        icon = QIcon()
        icon.addPixmap(QPixmap(".\Resources\eye.ico"), QIcon.Normal, QIcon.Off)
        self.setWindowIcon(icon)

    def model_is_changed(self):
        pass
