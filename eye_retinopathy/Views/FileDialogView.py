from PyQt5.QtWidgets import QFileDialog


class FileDialogView:

    @staticmethod
    def open_images_file_dialog(parent, base_path=''):
        dialog = QFileDialog(parent)
        options = dialog.options()
        options |= dialog.DontUseCustomDirectoryIcons
        file_names,res = dialog.getOpenFileNames(parent, 'File selection', base_path, "Image files (*.jpg *.jpeg *tiff *.png)")
        return file_names, res

    @staticmethod
    def save_text_file_dialog(parent, base_path=''):
        dialog = QFileDialog(parent)
        options = dialog.options()
        options |= dialog.DontUseCustomDirectoryIcons
        file_name, file_type = dialog.getSaveFileName(parent, 'Save file', base_path, "Text files (*.txt)")
        return file_name