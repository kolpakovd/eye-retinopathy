from PyQt5.QtWidgets import QWidget, QListWidgetItem, QVBoxLayout, QLabel
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt

from ..Unilities import Observer
from ..UI.ImagesViewer import Ui_ImagesViewer


class QCustomListItem(QWidget):

    def __init__(self, parent=None):
        super(QCustomListItem, self).__init__(parent)
        self.image_path = ''
        self.lbl_image = QLabel()

        self.central_layout = QVBoxLayout()
        self.central_layout.addWidget(self.lbl_image, 0, Qt.AlignCenter)

        self.setLayout(self.central_layout)

    def set_image(self, image_path):
        self.image_path = image_path
        self.lbl_image.setPixmap(QPixmap(self.image_path).scaled(100, 100))


class ImagesViewerView(QWidget, Observer):

    def __init__(self, in_model, in_controller, parent=None):
        super(QWidget, self).__init__(parent)

        self.parent = parent
        self.model = in_model
        self.controller = in_controller

        self.ui = Ui_ImagesViewer()
        self.ui.setupUi(self)

        self.ui.lv_images.itemSelectionChanged.connect(self.controller.selected_item)

    def show_obj(self, obj):
        list_item = QCustomListItem()
        list_item.set_image(obj.file_name)

        item_widget = QListWidgetItem(self.ui.lv_images)
        item_widget.setSizeHint(list_item.sizeHint())
        item_widget.setToolTip(obj.file_name)

        self.ui.lv_images.addItem(item_widget)
        self.ui.lv_images.setItemWidget(item_widget, list_item)

    def clear_list(self):
        self.ui.lv_images.clear()

    def model_is_changed(self):
        self.clear_list()
        for o in self.model.get_eyes():
            self.show_obj(o)
