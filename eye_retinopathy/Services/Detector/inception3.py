import tensorflow as tf
import os

from .slim import slim


class InceptionV3:

    def __init__(self, ):
        self.num_classes = 6
        batch_norm_params = {
            'decay': 0.9997,
            'epsilon': 0.001,
        }

        self.input = tf.placeholder(tf.float32, (None, 299, 299, 3))
        image = tf.expand_dims(self.input, 0)
        image = tf.squeeze(image, [0])
        image = tf.subtract(image, 0.5)
        self.image = tf.multiply(image, 2.0)
        with slim.arg_scope([slim.ops.conv2d, slim.ops.fc], weight_decay=0.00004):
            with slim.arg_scope([slim.ops.conv2d], stddev=0.1, activation=tf.nn.relu, batch_norm_params=batch_norm_params):
                logits, endpoints = slim.inception.inception_v3(self.image , dropout_keep_prob=0.8, num_classes=6, is_training=False, restore_logits=True, scope=None)
                self._probabilities = tf.nn.softmax(logits)

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self._sess = tf.Session(config=config)

    def load(self, weights=os.path.join(os.path.split(__file__)[0], r'Models\model.ckpt-90000')):
        saver = tf.train.Saver(tf.get_collection(slim.variables.VARIABLES_TO_RESTORE))
        saver.restore(self._sess, weights)

    def predict(self, images):
        result, sh = self._sess.run([self._probabilities, self.image], feed_dict={self.input: [images]})
        return result
