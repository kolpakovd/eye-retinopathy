import numpy as np
import pandas as pd

from .inception3 import InceptionV3

Dr_cls_lbl = {
    0: 'No DR',
    1: 'Mild',
    2: 'Moderate',
    3: 'Severe',
    4: 'Proliferative DR'
}


class DiabeticRetinopathyDetector:

    def __init__(self, inception):
        assert type(inception) is InceptionV3

        self._labels = ['None', ]
        self._labels.extend(list(Dr_cls_lbl.values()))

        self._inception = inception
        self._inception.load()

    def get_predict(self, images):
        images = np.array(images)

        if len(images.shape) == 3:
            images = np.expand_dims(images, 0)

        for image in images:
            assert len(image.shape) == 3

            probability = self._inception.predict(image)[0]
            results = pd.DataFrame({'class': self._labels, 'probability': probability})
            results.sort_values(by=['probability'], ascending=False, inplace=True)
            yield results
