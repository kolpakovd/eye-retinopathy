from .imagenet_detecor import ImageNetDetector
from .dr_detector import DiabeticRetinopathyDetector
from .inception3 import InceptionV3
