from .Detector import (InceptionV3, ImageNetDetector, DiabeticRetinopathyDetector)

DETECTOR = DiabeticRetinopathyDetector(InceptionV3())
