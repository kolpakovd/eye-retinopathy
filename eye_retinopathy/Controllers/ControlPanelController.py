from PyQt5.QtWidgets import QMessageBox

from ..Views import ControlPanelView
from ..Views import FileDialogView
from ..Models import EyeObject


class ControlPanelController:

    def __init__(self, in_model, parent=None):
        self.parent = parent
        self.model = in_model
        self.view = ControlPanelView(in_model, self, parent)
    
    def clear_list_clicked(self):
        self.model.reset()
        self.parent.controller.images_list_cleared()
    
    def diagnose_clicked(self):
        self.model.get_hr_diagnosis()
        self.parent.controller.diagnosis_is_changed()

    def load_image_clicked(self):
        file_names, _ = FileDialogView.open_images_file_dialog(self.parent)
        if file_names:
            for file_name in file_names:
                self.parent.controller.add_eye(file_name)

    def save_diagnosis_clicked(self):
        diagnosis = self.model.get_hr_diagnosis()
        if not diagnosis:
            mb = QMessageBox(self.view)
            mb.warning(self.view, 'Warning', 'No diagnosis!')
            return
        file = FileDialogView.save_text_file_dialog(self.view)
        if file:
            with open(file, 'w') as f:
                f.writelines(str(diagnosis))
            self.parent.controller.diagnosis_saved(file)
