from ..Views import MainWindowView, FileDialogView
from ..Controllers import DiagnosisPanelController, ControlPanelController, ImagesViewerPanelController, SelectedImageViewerController
from ..Models import EyeObject

class MainWindowController:

    def __init__(self, in_model, parent=None):
        self.model = in_model
        self.view = MainWindowView(in_model, self, parent)

        self.diagnosis_panel_controller = DiagnosisPanelController(in_model, self.view)
        self.control_panel_controller = ControlPanelController(in_model, self.view)
        self.images_viewer_controller = ImagesViewerPanelController(in_model, self.view)
        self.selected_image_viewer_controller = SelectedImageViewerController(in_model, self.view)

        self.view.setup_panels()
        self.view.show()

    def images_list_cleared(self):
        self.diagnosis_is_changed()
        self.selected_item_changed()
        self.list_of_images_is_changed()
        self.view.statusBar().showMessage('Images list cleared', -1)

    def diagnosis_is_changed(self):
        self.diagnosis_panel_controller.view.model_is_changed()
        self.view.statusBar().showMessage('Diagnosis received', -1)

    def list_of_images_is_changed(self):
        self.images_viewer_controller.view.model_is_changed()
        self.view.statusBar().showMessage('Updated image list', -1)

    def selected_item_changed(self):
        self.selected_image_viewer_controller.view.model_is_changed()

    def diagnosis_saved(self, file):
        self.view.statusBar().showMessage('Diagnosis saved in file: %s' % file.replace('/', '\\'), -1)

    def load_image_clicked(self):
        self.control_panel_controller.load_image_clicked()

    def clear_list_clicked(self):
        self.control_panel_controller.clear_list_clicked()

    def diagnose_clicked(self):
        self.control_panel_controller.diagnose_clicked()

    def save_diagnosis_clicked(self):
        self.control_panel_controller.save_diagnosis_clicked()

    def add_eye(self, path):
        path = path.replace('file:///', '')
        self.model.add_eye(EyeObject(path))
        self.list_of_images_is_changed()