from ..Views import ImagesViewerView


class ImagesViewerPanelController:

    def __init__(self, in_model, parent=None):
        self.parent = parent
        self.model = in_model
        self.view = ImagesViewerView(in_model, self, parent)

    def selected_item(self):
        selected_items = self.view.ui.lv_images.selectedItems()
        if selected_items:
            img_path = selected_items[0].toolTip()
            for eye in self.model.get_eyes():
                if eye.file_name == img_path:
                    self.model.selected_eye = eye
                    self.parent.controller.selected_item_changed()
        if not self.model.selected_eye:
            self.parent.controller.selected_item_changed()
