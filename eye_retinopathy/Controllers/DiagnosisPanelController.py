from ..Views import DiagnosisPanelView


class DiagnosisPanelController:

    def __init__(self, in_model, parent=None):
        self.parent = parent
        self.model = in_model
        self.view = DiagnosisPanelView(in_model, self, parent)
