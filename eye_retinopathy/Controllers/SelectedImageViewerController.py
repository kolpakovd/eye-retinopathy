from ..Views import SelectedImageViewerView
from ..Models import EyeObject


class SelectedImageViewerController:

    def __init__(self, in_model, parent=None):
        self.parent = parent
        self.model = in_model
        self.view = SelectedImageViewerView(in_model, self, parent)
        self._previous_slider_value = 100

    def changed_slider_value(self):
        new_value = round(self.view.ui.slider_zoom.value() / 10) * 10
        self.view.ui.slider_zoom.setValue(new_value)
        self.view.ui.lbl_zoom.setText(str(new_value) + '%')

        if new_value - self._previous_slider_value > 0:
            self.view.zoom_reversed_hint(new_value, 1.1)
        elif new_value == self._previous_slider_value:
            return
        else:
            self.view.zoom_reversed_hint(new_value, 0.9)

        self._previous_slider_value = new_value

    def zoom_p_clicked(self):
        new_value = self.view.ui.slider_zoom.value() + 10
        self.view.ui.slider_zoom.setValue(new_value)
        self.view.zoom_reversed_hint(new_value, 1.1)

    def zoom_m_clicked(self):
        new_value = self.view.ui.slider_zoom.value() - 10
        self.view.ui.slider_zoom.setValue(new_value)
        self.view.zoom_reversed_hint(new_value, 0.9)
    
    def add_eye_hook(self, path):
        self.parent.controller.add_eye(path)
