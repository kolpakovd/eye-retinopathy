import sys
from PyQt5.QtWidgets import QApplication

from eye_retinopathy.Controllers import MainWindowController
from eye_retinopathy.Models import Content

APP = QApplication(sys.argv)


def main():
    MainWindowController(Content())
    sys.exit(APP.exec_())


if __name__ == '__main__':
    main()
