# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Projects\eye-retinopathy\eye_retinopathy\UI\ControlPanel.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ControlPanel(object):
    def setupUi(self, ControlPanel):
        ControlPanel.setObjectName("ControlPanel")
        ControlPanel.resize(410, 55)
        self.gridLayout = QtWidgets.QGridLayout(ControlPanel)
        self.gridLayout.setContentsMargins(5, 5, 5, 5)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setContentsMargins(5, 5, 5, 5)
        self.horizontalLayout_2.setSpacing(10)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.btn_diagnose = QtWidgets.QPushButton(ControlPanel)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.btn_diagnose.setFont(font)
        self.btn_diagnose.setObjectName("btn_diagnose")
        self.horizontalLayout_2.addWidget(self.btn_diagnose)
        self.btn_save_diagnosis = QtWidgets.QPushButton(ControlPanel)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.btn_save_diagnosis.setFont(font)
        self.btn_save_diagnosis.setObjectName("btn_save_diagnosis")
        self.horizontalLayout_2.addWidget(self.btn_save_diagnosis)
        self.btn_load_image = QtWidgets.QPushButton(ControlPanel)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.btn_load_image.setFont(font)
        self.btn_load_image.setObjectName("btn_load_image")
        self.horizontalLayout_2.addWidget(self.btn_load_image)
        self.btn_clear_list = QtWidgets.QPushButton(ControlPanel)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.btn_clear_list.setFont(font)
        self.btn_clear_list.setObjectName("btn_clear_list")
        self.horizontalLayout_2.addWidget(self.btn_clear_list)
        self.gridLayout.addLayout(self.horizontalLayout_2, 0, 0, 1, 1)

        self.retranslateUi(ControlPanel)
        QtCore.QMetaObject.connectSlotsByName(ControlPanel)

    def retranslateUi(self, ControlPanel):
        _translate = QtCore.QCoreApplication.translate
        ControlPanel.setWindowTitle(_translate("ControlPanel", "ControlPanel"))
        self.btn_diagnose.setText(_translate("ControlPanel", "Diagnose"))
        self.btn_save_diagnosis.setText(_translate("ControlPanel", "Save diagnosis"))
        self.btn_load_image.setText(_translate("ControlPanel", "Load image"))
        self.btn_clear_list.setText(_translate("ControlPanel", "Clear list"))


