from .ControlPanel import Ui_ControlPanel
from .DiagnosisPanel import Ui_DiagnosisPanel
from .ImagesViewer import Ui_ImagesViewer
from .SelectedImageViewer import Ui_SelectedImageViewer
from .MainWindow import Ui_MainWindow
