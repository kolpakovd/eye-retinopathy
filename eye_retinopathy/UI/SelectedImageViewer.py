# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Projects\eye-retinopathy\eye-retinopathy\eye_retinopathy\UI\SelectedImageViewer.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_SelectedImageViewer(object):
    def setupUi(self, SelectedImageViewer):
        SelectedImageViewer.setObjectName("SelectedImageViewer")
        SelectedImageViewer.resize(610, 549)
        self.gridLayout_2 = QtWidgets.QGridLayout(SelectedImageViewer)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gb_viewer = QtWidgets.QGroupBox(SelectedImageViewer)
        self.gb_viewer.setTitle("")
        self.gb_viewer.setObjectName("gb_viewer")
        self.gridLayout = QtWidgets.QGridLayout(self.gb_viewer)
        self.gridLayout.setContentsMargins(0, 0, 0, 1)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.gv_image = QtWidgets.QGraphicsView(self.gb_viewer)
        self.gv_image.setObjectName("gv_image")
        self.verticalLayout.addWidget(self.gv_image)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.lbl_zoom = QtWidgets.QLabel(self.gb_viewer)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_zoom.sizePolicy().hasHeightForWidth())
        self.lbl_zoom.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.lbl_zoom.setFont(font)
        self.lbl_zoom.setObjectName("lbl_zoom")
        self.horizontalLayout.addWidget(self.lbl_zoom)
        self.btn_zoom_m = QtWidgets.QPushButton(self.gb_viewer)
        self.btn_zoom_m.setAutoFillBackground(True)
        self.btn_zoom_m.setText("")
        self.btn_zoom_m.setIconSize(QtCore.QSize(15, 15))
        self.btn_zoom_m.setObjectName("btn_zoom_m")
        self.horizontalLayout.addWidget(self.btn_zoom_m)
        self.slider_zoom = QtWidgets.QSlider(self.gb_viewer)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.slider_zoom.sizePolicy().hasHeightForWidth())
        self.slider_zoom.setSizePolicy(sizePolicy)
        self.slider_zoom.setMinimumSize(QtCore.QSize(140, 0))
        self.slider_zoom.setMinimum(20)
        self.slider_zoom.setMaximum(200)
        self.slider_zoom.setSingleStep(10)
        self.slider_zoom.setProperty("value", 100)
        self.slider_zoom.setOrientation(QtCore.Qt.Horizontal)
        self.slider_zoom.setTickPosition(QtWidgets.QSlider.TicksBelow)
        self.slider_zoom.setTickInterval(10)
        self.slider_zoom.setObjectName("slider_zoom")
        self.horizontalLayout.addWidget(self.slider_zoom)
        self.btn_zoom_p = QtWidgets.QPushButton(self.gb_viewer)
        self.btn_zoom_p.setAutoFillBackground(True)
        self.btn_zoom_p.setText("")
        self.btn_zoom_p.setIconSize(QtCore.QSize(15, 15))
        self.btn_zoom_p.setObjectName("btn_zoom_p")
        self.horizontalLayout.addWidget(self.btn_zoom_p)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.gridLayout_2.addWidget(self.gb_viewer, 0, 0, 1, 1)

        self.retranslateUi(SelectedImageViewer)
        QtCore.QMetaObject.connectSlotsByName(SelectedImageViewer)

    def retranslateUi(self, SelectedImageViewer):
        _translate = QtCore.QCoreApplication.translate
        SelectedImageViewer.setWindowTitle(_translate("SelectedImageViewer", "SelectedImageViewer"))
        self.lbl_zoom.setText(_translate("SelectedImageViewer", "100%"))


