# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\Projects\eye-retinopathy\eye_retinopathy\UI\DiagnosisPanel.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_DiagnosisPanel(object):
    def setupUi(self, DiagnosisPanel):
        DiagnosisPanel.setObjectName("DiagnosisPanel")
        DiagnosisPanel.resize(385, 248)
        self.verticalLayout = QtWidgets.QVBoxLayout(DiagnosisPanel)
        self.verticalLayout.setObjectName("verticalLayout")
        self.txt_diagnosis = QtWidgets.QTextEdit(DiagnosisPanel)
        self.txt_diagnosis.setReadOnly(True)
        self.txt_diagnosis.setObjectName("txt_diagnosis")
        self.verticalLayout.addWidget(self.txt_diagnosis)

        self.retranslateUi(DiagnosisPanel)
        QtCore.QMetaObject.connectSlotsByName(DiagnosisPanel)

    def retranslateUi(self, DiagnosisPanel):
        _translate = QtCore.QCoreApplication.translate
        DiagnosisPanel.setWindowTitle(_translate("DiagnosisPanel", "DiagnosisPanel"))


