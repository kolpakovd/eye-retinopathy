# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Projects\eye-retinopathy\eye-retinopathy\eye_retinopathy\UI\ImagesViewer.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ImagesViewer(object):
    def setupUi(self, ImagesViewer):
        ImagesViewer.setObjectName("ImagesViewer")
        ImagesViewer.resize(222, 495)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(ImagesViewer.sizePolicy().hasHeightForWidth())
        ImagesViewer.setSizePolicy(sizePolicy)
        self.gridLayout = QtWidgets.QGridLayout(ImagesViewer)
        self.gridLayout.setObjectName("gridLayout")
        self.lv_images = QtWidgets.QListWidget(ImagesViewer)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lv_images.sizePolicy().hasHeightForWidth())
        self.lv_images.setSizePolicy(sizePolicy)
        self.lv_images.setObjectName("lv_images")
        self.gridLayout.addWidget(self.lv_images, 0, 0, 1, 1)

        self.retranslateUi(ImagesViewer)
        QtCore.QMetaObject.connectSlotsByName(ImagesViewer)

    def retranslateUi(self, ImagesViewer):
        _translate = QtCore.QCoreApplication.translate
        ImagesViewer.setWindowTitle(_translate("ImagesViewer", "Form"))


