import numpy as np
import tensorflow as tf

from trainer.config import *
import pandas as pd
import os
import shutil
import glob

FLAGS = tf.app.flags.FLAGS


def move_scr():
    train_dir = r"G:\Diabetic retinopathy\test"
    label_df = pd.read_csv(TEST_HEADER_FILE_PATH)

    for i, row in label_df.iterrows():
        shutil.move(os.path.join(train_dir + '_old', str(row['image']) + '.jpeg'), os.path.join(train_dir + '/' + CLASS_NAMES[row['level']], str(row['image']) + '.jpeg'))


def main(_):
    from trainer.inception import diabetic_data
    from trainer.inception import inception_eval

    dataset = diabetic_data.DiabeticRetinopathyDataset(subset=FLAGS.subset)

    # from trainer.inception import inception_train
    assert dataset.data_files()
    # inception_train.train(dataset)
    # assert dataset.data_files()

    inception_eval.evaluate(dataset)


if __name__ == '__main__':
    tf.app.run()