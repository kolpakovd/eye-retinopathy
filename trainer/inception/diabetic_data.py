from .dataset import Dataset


class DiabeticRetinopathyDataset(Dataset):
    """Docs for DiabeticRetinopathy"""

    def __init__(self, subset):
        """Constructor for DiabeticRetinopathy"""
        super(DiabeticRetinopathyDataset, self).__init__('DiabeticRetinopathy', subset)

    def num_classes(self):
        return 5

    def num_examples_per_epoch(self):
        if self.subset == 'train':
            return 35122
        if self.subset == 'validation':
            return 53570

    def download_message(self):
        print('Download done')